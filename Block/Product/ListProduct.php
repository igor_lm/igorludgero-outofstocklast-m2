<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 21/12/16
 * Time: 16:27
 */

namespace Igorludgero\Outofstocklast\Block\Product;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct{

    protected function _getProductCollection(){
        $collection = parent::_getProductCollection();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productIds = $objectManager->create('\Igorludgero\Outofstocklast\Helper\Data')->getIdsToLastStock($collection->getAllIds());
        $orderString = array('CASE e.entity_id');
        foreach($productIds as $i => $productId) {
            $orderString[] = 'WHEN '.$productId.' THEN '.$i;
        }
        $orderString[] = 'END';
        $orderString = implode(' ', $orderString);
        $collection->addAttributeToFilter('entity_id', array('in' => $productIds));
        $collection->getSelect()->order(new \Zend_Db_Expr($orderString));
        return $collection;
    }

}